#!/usr/bin/python3

from des import *

base_path = 'messages_to_decrypt/'
clefs = ['Clef_de_Bible.txt', 'Clef_de_Coran.txt', 'Clef_de_Kaamelot.txt', 'Clef_de_Martin.txt', 'Clef_de_Orelsan.txt']
mdess = ['Chiffrement_DES_de_Bible.txt', 'Chiffrement_DES_de_Coran.txt', 'Chiffrement_DES_de_Kaamelot.txt',
         'Chiffrement_DES_de_Martin.txt', 'Chiffrement_DES_de_Orelsan.txt']

list_decrypted = []
i = 0

for i in range(len(clefs)):
    with open(base_path + clefs[i], 'r', encoding="latin-1") as f:
        r = str(f.read())
        r = r.split('\n')[0]
        r = r.split('\r')[0]
        with open(base_path + mdess[i], 'r', encoding="latin-1") as f2:
            r2 = f2.read()
            d = DES(key=r, encrypt_method='DECRYPT')
            d.encrypt(text=r2)
            s_a = base_path + 'res/' + clefs[i] + mdess[i]
            a = open(s_a, 'x', encoding='latin-1')
            a.write(d.result)
            a.close()
    i += 1
