#!/usr/bin/python3

from des import *
DEBUG = True


def write_to_file(filename, text_to_write):
    f = open(filename, 'w')
    f.write(text_to_write)
    f.close()


def get_key():
    choice = input('I need a key to continue, it is in a FILE (1) or you want to TYPE (2) it ?')
    text = ''
    if choice == '1':
        filepath = input('Where ? ')
        with open(filepath) as f:
            for line in f:
                text += line
    elif choice == '2':
        text = input('Type the key : ')
    else:
        return None
    return text


def get_debug_key():
    f = open("resources/messages/Clef1.txt", "r")
    k = f.read()
    f.close()
    return k


def create_empty_matrix(row=1, column=1):
    return [[0 for col in range(column)] for r in range(row)]


def apply_exclusive_add(data=None, key=None, zf=48):
    if not data or not key:
        return ''
    return bin(int(data, 2) ^ int(key, 2))[2:].zfill(zf)


def byte_shift(bits=None):
    if not bits:
        return None
    bits = list(bits)
    return ''.join(bits[1:] + [bits[0]])


def byte_shift_reversed(bits=None):
    if not bits:
        return None
    bits = list(bits)
    return ''.join([bits[-1]] + bits[0:])


def message_package(message=None, padding=64):
    if not message:
        return []
    message_list, threshold, counter = [], len(message), 0
    while threshold > 0:
        m = message[counter * padding:(counter+1) * padding]
        if len(m) != padding:
            diff = padding - len(m)
            for j in range(diff):
                m = m + '0'
        if len(m) > 0:
            message_list.append(m)
        threshold -= padding
        counter += 1
    return message_list


def text_to_binary(text=None):
    if not text:
        return ''
    return ''.join(format(ord(x), 'b').zfill(8) for x in text)


def split_string(s=None, nb_blocks=1, nb_bits=None):
    if not s or not nb_bits:
        return []
    r = []
    for j in range(nb_blocks):
        a, b = j * nb_bits, (j + 1) * nb_bits
        r.append(s[a:b])


def binary_to_word(text=''):
    p, li, r, counter = 7, [], '', 0
    for letter in text:
        li.append(letter)
        if counter % p == 0 and counter != 0:
            r += transform_bit_array_to_char(byte_array=li)
            li, counter = [], -1
        counter += 1
    if len(li) != 0:
        li = ['0' for i in range(8-len(li))] + li
        r += transform_bit_array_to_char(byte_array=li)
    return r


def transform_bit_array_to_char(byte_array=None):
    if not byte_array:
        return None
    t = ''.join(byte_array)
    b = int(t, 2)
    if b == 0:
        return ''
    return chr(b)


def is_binary_text(text=None):
    if not text:
        return False
    for c in list(text):
        if c != '0' and c != '1':
            return False
    return True


def split_in(nb_blocks=8, nb_bits=6, data=''):
    li, n = [], 0
    while n < nb_blocks * nb_bits:
        t = ''
        for i in range(nb_bits):
            t += data[i + n]
        li.append(t)
        n += nb_blocks
    return li


def get_bit_sequences(sequence=None, nb=0):
    if not sequence:
        return None
    nb, res, tmp = nb if nb > 0 else 0, [], ''
    for bit in sequence:
        tmp += bit
        if len(tmp) == nb:
            res.append(tmp)
            tmp = ''
    return res


def get_blocks_of_message(t=None, using_alphabet=False):
    if not t:
        return []
    if using_alphabet:
        return message_package(message=alphabet_conv_bin(t) if is_binary_text(text=t) is False else t)
    else:
        return message_package(message=text_to_binary(t) if is_binary_text(text=t) is False else t)
