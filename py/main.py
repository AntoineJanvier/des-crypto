#!/usr/bin/python3

import sys
from des import *


if __name__ == '__main__':
    DEBUG = True if len(sys.argv) > 1 and sys.argv[1] == 'DEBUG' else False
    d = DES(debug=DEBUG)
    d.process()
