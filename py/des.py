#!/usr/bin/python3

from resources.utils import *
from resources.constantes import Constantes
from resources.conv_bin import *


class DES(object):

    def __init__(self, debug=False, file_in=None, file_out=None, key=None, key_clean=None, encrypt_method=None,
                 out_type=None):
        self.DEBUG = debug
        self.file_in = file_in
        self.file_out = file_out
        self.key = key
        self.key_clean = key_clean
        self.constantes = Constantes().get_constantes()
        self.result = None
        self.encrypt_or_decrypt = encrypt_method
        self.output_type = out_type

    def process(self):
        """
        Main function, user have to specify if he want to encrypt or decrypt, then go to '_prepare()'
        """
        type_of_use = '1' if self.DEBUG else input('Hello, do you want to ENCRYPT (1), DECRYPT (2) or QUIT (3) ? ')
        self.key = get_debug_key() if self.DEBUG else get_key()
        if type_of_use == '1':
            self.encrypt_or_decrypt = 'ENCRYPT'
            self._prepare(t='ENCRYPTION')
        elif type_of_use == '2':
            self.encrypt_or_decrypt = 'DECRYPT'
            self._prepare(t='DECRYPTION')
        elif type_of_use == '3':
            print('Goodbye !')
        else:
            print('Bad entry...')
        self.save_result()

    def _prepare(self, t):
        """
        User have to chose if he want to save the result (encryption or decryption) in a file or printed.
        If he chose file, he have to specify it.
        Then, he have to chose the input: a file or direct input - process the input in '_file()' or '_input()'.
        :param t: Text to encrypt or decrypt
        """
        choice = '0' if self.DEBUG else input('Result will be saved in a FILE (1) or just PRINTED (ENTER) ? ')
        if choice == '1':
            f = input('Output file : ')
            self.output_type = 'FILE'
            self.file_out = f
        else:
            self.output_type = 'LOG'
        a = '2' if self.DEBUG else input('You choose ' + t + ', on what ? FILE (1) or DIRECT_INPUT (2) ? ')
        if a == '1':
            file_to_process = input('On what file ? ')
            self.__file(filename=file_to_process)
        elif a == '2':
            self.__input(t='encrypt' if self.encrypt_or_decrypt == 'ENCRYPT' else 'decrypt')
        else:
            print('Bad entry...')

    def __file(self, filename):
        """
        Open the file, get the content, encrypt or decrypt it
        :param filename: File name that contains the content to encrypt or decrypt
        """
        self.result = ''
        with open(filename) as f:
            r = f.read()
            self.encrypt(text=r)

    def __input(self, t='encrypt'):
        """
        Get the input from user and encrypt or decrypt
        :param t: Method (encrypt or decrypt)
        """
        to_process = 'test' if self.DEBUG else input('Type what you want to ' + t + ' : ')
        self.result = ''
        self.encrypt(text=to_process)

    def encrypt(self, text='', using_alphabet=False):
        """
        Apply DES algorithm to 'text' and save result in 'self.result'
        :param using_alphabet: Use alphabet or direct binary conversion
        :param text: Text to encrypt or decrypt
        """
        encrypted_message = ''
        m = self.key_permutation(const_name='CP_1', key=self.key)
        keys = self.generate_keys(table=m)

        blocks_of_message = get_blocks_of_message(t=text, using_alphabet=using_alphabet)

        for block in blocks_of_message:
            block = self.key_permutation(const_name='PI', key=block)

            g, d = block[:32], block[32:]

            for idx in self.get_range_by_encryption_type():
                e_d = self.key_permutation(const_name='E', key=d)
                e_d_add_kk = apply_exclusive_add(data=e_d, key=keys[idx], zf=48)
                sed_k = self.get_sed_k(d=e_d_add_kk)
                psed_k = self.key_permutation(const_name='PERM', key=sed_k)

                old_d = d
                d = apply_exclusive_add(data=psed_k, key=g, zf=32)
                g = old_d
            tt = self.key_permutation(const_name='PI_I', key=d+g)
            tt = self.get_text_to_add_by_encryption_type(t=tt, using_alphabet=using_alphabet)
            encrypted_message += tt
        self.result = encrypted_message if not self.result else self.result + encrypted_message

    def get_text_to_add_by_encryption_type(self, t=None, using_alphabet=False):
        """
        If the decryption is wanted, convert the binary text to alphanumeric text
        :param using_alphabet: Use alphabet or direct binary conversion
        :param t: Text
        :return: Text
        """
        if not t:
            return ''
        if self.encrypt_or_decrypt == 'ENCRYPT':
            return t
        elif self.encrypt_or_decrypt == 'DECRYPT':
            if using_alphabet:
                r = alphabet_nib_vnoc(t)
            else:
                r = binary_to_word(t)
            return r
        else:
            return ''

    def get_range_by_encryption_type(self):
        """
        Range order depending on the method specified, from 0 to 15 if encryption and the reversed if decryption
        :return: Range list
        """
        if self.encrypt_or_decrypt == 'ENCRYPT':
            rr = range(0, 16)
        elif self.encrypt_or_decrypt == 'DECRYPT':
            rr = reversed(range(0, 16))
        else:
            rr = None
        return rr

    def get_sed_k(self, d=None):
        """
        Substitution matrix application
        :param d: Matrix 8x6
        :return: Matrix 8x4
        """
        if not d:
            return None
        e_d = []
        for i in range(8):
            n = d[i * 6:(i + 1) * 6]
            _a, _b = int(n[0] + n[5], 2) * 16, int(n[1] + n[2] + n[3] + n[4], 2)
            idx = _a + _b
            _c = bin(self.constantes['S' + str(i + 1)][idx])[2:].zfill(4)
            e_d.append(_c)
        return ''.join(e_d)

    def generate_keys(self, table=None):
        """
        Generate the 16 keys
        :param table: Key
        :return: List of the 16 keys generated with the key
        """
        k, g, d = [], table[:28], table[28:]
        for i in range(16):
            g, d = byte_shift(bits=g), byte_shift(bits=d)
            k.append(self.key_permutation(const_name='CP_2', key=g + d))
        return k

    def key_permutation(self, const_name=None, key=None):
        """
        Apply a const permutation matrix
        :param const_name: Name of the permutation matrix
        :param key: The matrix that will be confound with the substitution matrix
        :return: New matrix, mix of const and key
        """
        if not const_name or not key:
            return None
        r, cp = [], self.constantes[const_name]
        for i in range(len(cp)):
            r.append(key[cp[i]-1])
        return ''.join(r)

    def save_result(self):
        """
        The way to get the result
        """
        if not self.encrypt_or_decrypt or not self.output_type:
            print('Can not save...')
        else:
            if self.output_type == 'LOG':
                print(self.result)
            elif self.output_type == 'FILE':
                write_to_file(self.file_out, self.result)
                print('Saved !')
            else:
                print('Do not know where to save your result...')
