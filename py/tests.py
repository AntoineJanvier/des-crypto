#!/usr/bin/python3

import unittest
from des import *
from resources.utils import *
from resources.conv_bin import *


class TestDESMethods(unittest.TestCase):

    des = DES()
    base_key = '0101111001011011010100100111111101010001000110101011110010010001'
    clean_key = '01011110101101010100101111110101000000110110111101001000'

    def test_exclusive_add(self):
        a = '001111111111110110100100000000000111111110100100'
        b = '111110011000001010001110010101111111000011101001'
        exclusive_add = apply_exclusive_add(data=a, key=b)

        self.assertEqual('110001100111111100101010010101111000111101001101', exclusive_add,
                         msg='Bad operation when add exclusively')

    def test_second_key_permutation(self):
        self.des.key = self.base_key
        p = self.des.key_permutation(const_name='CP_2',
                                     key='10000000001111101001000111110101111010010010110101111110')
        self.assertEqual('111110011000001010001110010101111111000011101001', p, msg='CP2 permutation failed')

    def test_first_key_permutation(self):
        self.des.key = self.base_key
        p = self.des.key_permutation(const_name='CP_1', key=self.des.key)
        self.assertEqual('11000000000111110100100011110010111101001001011010111111', p, msg='CP1 permutation failed')

    def test_byte_shift_result(self):
        shift = byte_shift(bits='0110')
        self.assertEqual(shift, '1100', msg='Byte shift not working')

    def test_byte_shift_len(self):
        shift = byte_shift(bits=self.base_key)
        self.assertEqual(len(shift), len(self.base_key))

    def test_text_to_binary(self):
        a = 'abc'
        b = '011000010110001001100011'
        self.assertEqual(b, text_to_binary(a), msg='Conversion not working')

    def test_binary_to_word(self):
        a = 'abc'
        b = '011000010110001001100011'
        self.assertEqual(a, binary_to_word(b), msg='Conversion not working')

    def test_convert_alpha_to_binary_letter(self):
        a = 'C'
        self.assertEqual('000010', alphabet_conv_bin(a), msg='Conversion not working')

    def test_convert_binary_to_alpha_letter(self):
        a = '000010'
        self.assertEqual('C', alphabet_nib_vnoc(a), msg='Conversion not working')

    def test_convert_alpha_to_binary_weird_chars(self):
        a = '!'
        self.assertEqual('110111', alphabet_conv_bin(a), msg='Conversion not working')

    def test_convert_binary_to_alpha_weird_char(self):
        a = '110111'
        self.assertEqual('!', alphabet_nib_vnoc(a), msg='Conversion not working')

    def test_convert_alpha_to_binary_string(self):
        a = 'Hello, I am Batman !'
        self.assertEqual('000111011110100101100101101000110110110100001000110100011010100110110100000001011010101'
                         '101100110011010100111110100110111', alphabet_conv_bin(a),
                         msg='Conversion not working')

    def test_convert_binary_to_alpha_string(self):
        a = '0001110111101001011001011010001101101101000010001101000110101001101101000000010110101011011001100110' \
            '10100111110100110111'
        self.assertEqual('Hello, I am Batman !', alphabet_nib_vnoc(a), msg='Conversion not working')

    def test_get_blocks_of_message_from_binary_message(self):
        m = '1101110010111011110001001101010111100110111101111100001000110010110111001011101111000100110101011110' \
            '0110111101111100001000110010110111001011101111000100110101011110011011110111110000100011001011011100' \
            '1011101111000100110101011110011011110111110000100011001011011100101110111100010011010101111001101111' \
            '0111110000100011001011011100101110111100010011010101111001101111011111000010001100101101110010111011' \
            '1100010011010101111001101111011111000010001100101101110010111011110001001101010111100110111101111100' \
            '0010001100101101110010111011110001001101010111100110111101111100001000110010110111001011101111000100' \
            '1101010111100110111101111100001000110010110111001011101111000100110101011110011011110111110000100011' \
            '0010'

        r = get_blocks_of_message(t=m)

        for n in r:
            self.assertEqual(64, len(n), msg='Bad block length')
        self.assertEqual(m[:64], r[0], msg='Wrong block')

    def test_get_blocks_of_message_from_alpha_message(self):
        m = 'Hello, I am a beautiful test !'

        r = get_blocks_of_message(t=m)

        for n in r:
            self.assertEqual(64, len(n), msg='Bad block length')

    def test_encrypt_then_decrypt(self):
        m = 'Bonjour, je suisMdrrr :!!!AA'
        des = DES(key=self.base_key, encrypt_method='ENCRYPT')

        des.encrypt(text=m)
        r = des.result
        des.result = ''
        des.encrypt_or_decrypt = 'DECRYPT'
        des.encrypt(text=r)

        self.assertEqual(m, des.result, msg='Not the same as expected, encryption OR decryption problem')

    def test_prof_conv_bin(self):
        a = 'bonjour !'
        b = alphabet_conv_bin(a)
        c = alphabet_nib_vnoc(b)

        self.assertEqual(a, c)


if __name__ == '__main__':
    unittest.main()
