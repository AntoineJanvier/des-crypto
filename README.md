# DES-crypto

Python program that uses DES encryption

## Installation & Execution

```bash
$ git clone git@gitlab.com:AntoineJanvier/des-crypto.git
$ cd des-crypto/py/
$ python3 main.py
```

You will be prompt for actions.

## Generate documentation

```bash 
$ pydoc -w des
```

> `-w` is to generate HTML file in current directory

> 'des' is for 'des.py' file

This will search for all function documentation in the file, and print it in a HTML file

## PyCharm

### Open project

To use PyCharm correctly, open the `py` folder in it as project.

### Unit tests

On top-right corner, `Edit configurations`, `+`, `Python tests`, `Unit tests`
